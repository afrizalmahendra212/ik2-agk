
SetWindowTitle("Hit Jelly")
SetWindowSize(768, 1028, 0)
SetVirtualResolution(768, 1028)

#include "jelly.agc"
#include "background.agc"

CreateBackground()
CreateSpriteJelly()

point as integer = 5

CreateText(1, "Point: " + Str(point))
SetTextSize(1,35)
SetTextColor(1,0,0,0,255)
SetTextPosition(1, 1, 1)


do
	if getpointerpressed()=1
		
		sprite = GetSpriteHit(GetPointerX(), GetPointerY())
		if sprite<>1
			DeleteSprite(sprite)
			point = point + 5
		endif
		SetTextString (1, "Point: " + Str(point))
		
	endif
	
	sync()
	
loop
