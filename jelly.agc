
function CreateSpriteJelly()
	LoadImage(2, "Jelly1.png")
	LoadImage(3, "Jelly2.png")
	LoadImage(4, "Jelly3.png")
	LoadImage(5, "Jelly4.png")
	LoadImage(6, "Jelly5.png")
	LoadImage(7, "Jelly6.png")
	
	for i = 2 to 100
		CreateSprite(i, Random(2, 7))
		SetSpriteShape(i, 1)
		SetSpritePosition(i, Random(0, 600), Random(0, 100))
		SetSpriteSize(i, 120, 120)
	
		SetSpritePhysicsOn(i, 2)
		SetSpritePhysicsVelocity(i, 100, 0)
	next
	
endfunction
